**END USER LICENSE AGREEMENT (EULA) FOR CHESS ICON SET**

Please read this End User License Agreement (EULA) carefully before using the Chess Icon Set. This EULA is a legal agreement between you (the "User") and [Your Name or Company Name] (the "Licensor") for the Chess Icon Set (the "Icons").

**1. License Grant:**

1.1. Personal Use: The Licensor grants the User a non-exclusive, non-transferable, royalty-free license to use the Icons for personal, non-commercial use, with proper attribution as described in Section 2.1. Personal use includes but is not limited to using the Icons for personal projects, personal websites, or personal social media accounts.

1.2. Commercial Use: The User may use the Icons for commercial purposes upon payment of a one-time fee of $50 to the Licensor. This fee entitles the User to a non-exclusive, non-transferable, royalty-free license to use the Icons for commercial projects, including but not limited to business websites, marketing materials, software applications, and merchandise.

**2. Attribution:**

2.1. Personal Use: When using the Icons for personal, non-commercial projects, the User must provide proper attribution to the Licensor. The attribution should be included in a prominent location, such as a credits section or the footer of a website, and must include the following text: "Chess Icons by [Your Name or Company Name], used under a Creative Commons Attribution License."

2.2. Commercial Use: When using the Icons for commercial projects, the User is not required to provide attribution.

**3. Restrictions:**

3.1. The User may not sublicense, sell, or distribute the Icons as standalone files or as part of a competing icon set.

3.2. The User may not use the Icons for any unlawful or unethical purpose.

3.3. The User may not modify, adapt, or create derivative works based on the Icons for redistribution or resale.

**4. Termination:**

4.1. The Licensor reserves the right to terminate this license at any time if the User breaches any of the terms and conditions outlined in this EULA.

4.2. Upon termination of the license, the User must cease all use of the Icons and delete or destroy any copies of the Icons in their possession.

**5. Ownership:**

The Icons remain the intellectual property of the Licensor. This EULA does not grant the User any rights to the Icons other than those explicitly stated herein.

**6. Disclaimer of Warranty:**

The Icons are provided "as is" without warranty of any kind, either express or implied, including, but not limited to, the implied warranties of merchantability and fitness for a particular purpose.

**7. Limitation of Liability:**

The Licensor shall not be liable for any direct, indirect, special, incidental, or consequential damages arising out of the use or inability to use the Icons.

**8. Governing Law:**

This EULA shall be governed by and construed in accordance with the laws of South Carolina.

**9. Entire Agreement:**

This EULA constitutes the entire agreement between the User and the Licensor regarding the Icons and supersedes all prior agreements and understandings.

**10. Contact Information:**

If you have any questions or need to contact the Licensor for any reason, you can do so at tylertrotter@gmail.com.

By downloading, using, or obtaining the Icons, the User agrees to be bound by the terms and conditions of this EULA. If the User does not agree with the terms and conditions, they should not use the Icons.

ChessNotepad.com
tylertrotter@gmail.com